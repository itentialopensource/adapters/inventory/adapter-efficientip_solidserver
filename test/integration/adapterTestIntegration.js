/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-efficientip_solidserver',
      type: 'EfficientipSolidserver',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const EfficientipSolidserver = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Efficientip_solidserver Adapter Test', () => {
  describe('EfficientipSolidserver Class Tests', () => {
    const a = new EfficientipSolidserver(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-efficientip_solidserver-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-efficientip_solidserver-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const ipamCreateIpamAddress6BodyParam = {
      address6_hostaddr: 'string',
      space_id: 2,
      space_name: 'string',
      device_id: 10,
      interface_id: 4,
      address6_mac_addr: 'string',
      address6_name: 'string',
      port_id: 2,
      class_parameters_to_delete: [
        'string'
      ],
      address6_class_name: 'string',
      address6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createIpamAddress6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamAddress6(ipamCreateIpamAddress6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamAddress6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamAddressBodyParam = {
      address_hostaddr: 'string',
      space_id: 1,
      space_name: 'string',
      check_is_server_ip: 9,
      static_id: 10,
      lease_id: 4,
      device_id: 3,
      interface_id: 5,
      port_id: 3,
      address_mac_addr: 'string',
      address_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      address_class_name: 'string',
      address_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createIpamAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamAddress(ipamCreateIpamAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamAlias6BodyParam = {
      alias6_hostaddr: 'string',
      address6_id: 5,
      address6_name: 'string',
      space_id: 5,
      space_name: 'string',
      alias6_type: 'aaaa',
      warnings: 'accept'
    };
    describe('#createIpamAlias6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamAlias6(ipamCreateIpamAlias6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamAlias6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamAliasBodyParam = {
      alias_hostaddr: 'string',
      address_id: 6,
      address_name: 'string',
      space_id: 2,
      space_name: 'string',
      alias_type: 'A',
      warnings: 'accept'
    };
    describe('#createIpamAlias - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamAlias(ipamCreateIpamAliasBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamNetwork6BodyParam = {
      parent_network6_id: 1,
      space_id: 10,
      space_name: 'string',
      network6_addr: 'string',
      network6_end_addr: 'string',
      network6_prefix: 'string',
      allow_tree_reparenting: 7,
      network6_is_terminal: 8,
      network6_lock_network_broadcast: 1,
      permit_invalid: 7,
      permit_no_block6: 8,
      relative_position: 7,
      row_state: 1,
      network6_name: 'string',
      network_level: 9,
      use_reversed_relative_position: 9,
      vlan_id: 1,
      vlsm_space_id: 1,
      vlsm_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      network6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      network6_class_name: 'string',
      warnings: 'accept'
    };
    describe('#createIpamNetwork6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamNetwork6(ipamCreateIpamNetwork6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamNetwork6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamNetworkBodyParam = {
      parent_network_id: 1,
      space_id: 7,
      space_name: 'string',
      network_addr: 'string',
      network_end_addr: 'string',
      network_mask: 'string',
      network_prefix: 'string',
      network_size: 9,
      allow_tree_reparenting: 8,
      network_is_terminal: 8,
      network_lock_network_broadcast: 9,
      permit_invalid: 6,
      permit_no_block: 2,
      relative_position: 10,
      row_state: 1,
      network_level: 3,
      network_name: 'string',
      use_reversed_relative_position: 10,
      vlan_id: 9,
      vlsm_space_id: 6,
      vlsm_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      network_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      network_class_name: 'string',
      warnings: 'accept'
    };
    describe('#createIpamNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamNetwork(ipamCreateIpamNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamPool6BodyParam = {
      pool6_end_ip_addr: 'string',
      space_id: 9,
      space_name: 'string',
      pool6_start_ip_addr: 'string',
      network6_id: 5,
      pool6_name: 'string',
      pool6_read_only: 5,
      class_parameters_to_delete: [
        'string'
      ],
      pool6_class_name: 'string',
      pool6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createIpamPool6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamPool6(ipamCreateIpamPool6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamPool6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamPoolBodyParam = {
      pool_end_ip_addr: 'string',
      pool_size: 9,
      space_id: 10,
      space_name: 'string',
      pool_start_ip_addr: 'string',
      network_id: 10,
      pool_name: 'string',
      pool_read_only: 1,
      class_parameters_to_delete: [
        'string'
      ],
      pool_class_name: 'string',
      pool_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createIpamPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamPool(ipamCreateIpamPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamCreateIpamSpaceBodyParam = {
      space_name: 'string',
      parent_space_id: 6,
      parent_space_name: 'string',
      space_description: 'string',
      space_is_template: 10,
      class_parameters_to_delete: [
        'string'
      ],
      space_class_name: 'string',
      space_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createIpamSpace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpamSpace(ipamCreateIpamSpaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'createIpamSpace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddress6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddress6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddress6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamAddress6BodyParam = {
      address6_hostaddr: 'string',
      address6_id: 9,
      space_id: 1,
      space_name: 'string',
      device_id: 10,
      interface_id: 9,
      address6_mac_addr: 'string',
      address6_name: 'string',
      port_id: 3,
      class_parameters_to_delete: [
        'string'
      ],
      address6_class_name: 'string',
      address6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateIpamAddress6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamAddress6(ipamUpdateIpamAddress6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamAddress6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddress6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddress6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddress6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddress6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddress6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddress6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddressCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamAddressBodyParam = {
      address_hostaddr: 'string',
      address_id: 8,
      space_id: 10,
      space_name: 'string',
      check_is_server_ip: 7,
      static_id: 5,
      lease_id: 5,
      device_id: 10,
      interface_id: 10,
      port_id: 1,
      address_mac_addr: 'string',
      address_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      address_class_name: 'string',
      address_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateIpamAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamAddress(ipamUpdateIpamAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddressInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAddressList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAddressList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAddressList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAlias6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAlias6Count(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAlias6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamAlias6BodyParam = {
      alias6_hostaddr: 'string',
      address6_id: 1,
      address6_name: 'string',
      alias6_id: 8,
      space_id: 6,
      space_name: 'string',
      alias6_type: 'AAAA',
      warnings: 'accept'
    };
    describe('#updateIpamAlias6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamAlias6(ipamUpdateIpamAlias6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamAlias6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAlias6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAlias6List(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAlias6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAliasCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAliasCount(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAliasCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamAliasBodyParam = {
      alias_hostaddr: 'string',
      address_id: 3,
      address_name: 'string',
      alias_id: 9,
      space_id: 4,
      space_name: 'string',
      alias_type: 'A',
      warnings: 'accept'
    };
    describe('#updateIpamAlias - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamAlias(ipamUpdateIpamAliasBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAliasList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAliasList(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAliasList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetwork6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetwork6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetwork6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamNetwork6BodyParam = {
      parent_network6_id: 6,
      space_id: 3,
      space_name: 'string',
      network6_addr: 'string',
      network6_end_addr: 'string',
      network6_id: 1,
      network6_prefix: 'string',
      allow_tree_reparenting: 5,
      network6_is_terminal: 3,
      network6_lock_network_broadcast: 6,
      permit_invalid: 4,
      permit_no_block6: 3,
      relative_position: 10,
      row_state: 2,
      network6_name: 'string',
      network_level: 1,
      use_reversed_relative_position: 1,
      vlan_id: 7,
      vlsm_space_id: 10,
      vlsm_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      network6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      network6_class_name: 'string',
      warnings: 'accept'
    };
    describe('#updateIpamNetwork6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamNetwork6(ipamUpdateIpamNetwork6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamNetwork6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetwork6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetwork6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetwork6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetwork6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetwork6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetwork6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetworkCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetworkCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetworkCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamNetworkBodyParam = {
      parent_network_id: 3,
      space_id: 9,
      space_name: 'string',
      network_addr: 'string',
      network_end_addr: 'string',
      network_id: 4,
      network_mask: 'string',
      network_prefix: 'string',
      network_size: 4,
      allow_tree_reparenting: 2,
      network_is_terminal: 2,
      network_lock_network_broadcast: 1,
      permit_invalid: 6,
      permit_no_block: 2,
      relative_position: 4,
      row_state: 1,
      network_level: 3,
      network_name: 'string',
      use_reversed_relative_position: 5,
      vlan_id: 6,
      vlsm_space_id: 1,
      vlsm_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      network_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      network_class_name: 'string',
      warnings: 'accept'
    };
    describe('#updateIpamNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamNetwork(ipamUpdateIpamNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetworkInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetworkInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetworkInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamNetworkList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamNetworkList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamNetworkList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPool6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPool6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPool6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamPool6BodyParam = {
      pool6_end_ip_addr: 'string',
      pool6_id: 3,
      space_id: 1,
      space_name: 'string',
      pool6_start_ip_addr: 'string',
      network6_id: 2,
      pool6_name: 'string',
      pool6_read_only: 3,
      class_parameters_to_delete: [
        'string'
      ],
      pool6_class_name: 'string',
      pool6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateIpamPool6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamPool6(ipamUpdateIpamPool6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamPool6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPool6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPool6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPool6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPool6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPool6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPool6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPoolCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPoolCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPoolCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamPoolBodyParam = {
      pool_end_ip_addr: 'string',
      pool_id: 1,
      pool_size: 4,
      space_id: 7,
      space_name: 'string',
      pool_start_ip_addr: 'string',
      network_id: 7,
      pool_name: 'string',
      pool_read_only: 8,
      class_parameters_to_delete: [
        'string'
      ],
      pool_class_name: 'string',
      pool_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateIpamPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamPool(ipamUpdateIpamPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPoolInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPoolInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPoolInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPoolList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPoolList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPoolList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamSpaceCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamSpaceCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamSpaceCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamUpdateIpamSpaceBodyParam = {
      space_id: 8,
      space_name: 'string',
      parent_space_id: 8,
      parent_space_name: 'string',
      space_description: 'string',
      space_is_template: 3,
      class_parameters_to_delete: [
        'string'
      ],
      space_class_name: 'string',
      space_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateIpamSpace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpamSpace(ipamUpdateIpamSpaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'updateIpamSpace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamSpaceInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamSpaceInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamSpaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamSpaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamSpaceList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamSpaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpGroupBodyParam = {
      server_id: 9,
      server_name: 'string',
      group_name: 'string',
      server_hostaddr: 'string',
      server_ip_id: 5,
      class_parameters_to_delete: [
        'string'
      ],
      group_class_name: 'string',
      group_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpGroup(dhcpCreateDhcpGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpRange6BodyParam = {
      server6_id: 8,
      server6_name: 'string',
      range6_end_addr: 'string',
      range6_start_addr: 'string',
      scope6_id: 8,
      server6_hostaddr: 'string',
      scope6_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      range6_class_name: 'string',
      range6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpRange6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpRange6(dhcpCreateDhcpRange6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpRange6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpRangeBodyParam = {
      server_id: 2,
      server_name: 'string',
      range_end_addr: 'string',
      range_start_addr: 'string',
      scope_id: 3,
      server_hostaddr: 'string',
      range_acl: 'string',
      range_name: 'string',
      scope_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      range_class_name: 'string',
      range_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpRange(dhcpCreateDhcpRangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpScope6BodyParam = {
      server6_id: 6,
      server6_name: 'string',
      scope6_end_addr: 'string',
      scope6_prefix: 'string',
      scope6_start_addr: 'string',
      server6_hostaddr: 'string',
      failover6_id: 6,
      failover6_name: 'string',
      scope6_name: 'string',
      scope6_space_id: 4,
      scope6_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      scope6_class_name: 'string',
      scope6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpScope6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpScope6(dhcpCreateDhcpScope6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpScope6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpScopeBodyParam = {
      server_id: 10,
      server_name: 'string',
      scope_net_addr: 'string',
      scope_net_mask: 'string',
      server_hostaddr: 'string',
      failover_id: 9,
      failover_name: 'string',
      scope_name: 'string',
      scope_space_id: 3,
      scope_space_name: 'string',
      sharednetwork_id: 10,
      sharednetwork_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      scope_class_name: 'string',
      scope_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpScope(dhcpCreateDhcpScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpSharednetworkBodyParam = {
      server_id: 3,
      server_name: 'string',
      sharednetwork_name: 'string',
      server_hostaddr: 'string',
      server_ip_id: 9,
      warnings: 'accept'
    };
    describe('#createDhcpSharednetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpSharednetwork(dhcpCreateDhcpSharednetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpSharednetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpStatic6BodyParam = {
      server6_id: 4,
      server6_name: 'string',
      static6_client_duid: 'string',
      static6_mac_addr: 'string',
      static6_name: 'string',
      server6_hostaddr: 'string',
      server6_ip_id: 2,
      group6_id: 8,
      group6_name: 'string',
      static6_addr: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      static6_class_name: 'string',
      static6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpStatic6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpStatic6(dhcpCreateDhcpStatic6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpStatic6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpCreateDhcpStaticBodyParam = {
      server_id: 6,
      server_name: 'string',
      static_mac_addr: 'string',
      server_hostaddr: 'string',
      server_ip_id: 9,
      group_id: 3,
      group_name: 'string',
      static_addr: 'string',
      static_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      static_class_name: 'string',
      static_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDhcpStatic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpStatic(dhcpCreateDhcpStaticBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'createDhcpStatic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclentryCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclentryCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclentryCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclentryInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclentryInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclentryInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpAclentryList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpAclentryList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpAclentryList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpFailoverCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpFailoverCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpFailoverCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpFailoverInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpFailoverInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpFailoverInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpFailoverList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpFailoverList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpFailoverList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpGroup6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpGroup6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpGroup6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpGroupCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpGroupCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpGroupCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpGroupBodyParam = {
      server_id: 6,
      server_name: 'string',
      group_id: 10,
      group_name: 'string',
      server_hostaddr: 'string',
      server_ip_id: 4,
      class_parameters_to_delete: [
        'string'
      ],
      group_class_name: 'string',
      group_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpGroup(dhcpUpdateDhcpGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpGroupInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpGroupInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpGroupInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpGroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpGroupList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpGroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLease6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLease6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpLease6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLease6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLease6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpLease6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLeaseCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLeaseCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpLeaseCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLeaseInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLeaseInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpLeaseInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpLeaseList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpLeaseList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpLeaseList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRange6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRange6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRange6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpRange6BodyParam = {
      server6_id: 4,
      server6_name: 'string',
      range6_end_addr: 'string',
      range6_id: 8,
      range6_start_addr: 'string',
      scope6_id: 4,
      server6_hostaddr: 'string',
      scope6_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      range6_class_name: 'string',
      range6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpRange6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpRange6(dhcpUpdateDhcpRange6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpRange6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRange6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRange6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRange6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRange6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRange6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRange6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRangeCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRangeCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRangeCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpRangeBodyParam = {
      server_id: 4,
      server_name: 'string',
      range_end_addr: 'string',
      range_id: 10,
      range_start_addr: 'string',
      scope_id: 4,
      server_hostaddr: 'string',
      range_acl: 'string',
      range_name: 'string',
      scope_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      range_class_name: 'string',
      range_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpRange(dhcpUpdateDhcpRangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRangeInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRangeInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRangeInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpRangeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpRangeList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpRangeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScope6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScope6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScope6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpScope6BodyParam = {
      server6_id: 5,
      server6_name: 'string',
      scope6_end_addr: 'string',
      scope6_id: 2,
      scope6_prefix: 'string',
      scope6_start_addr: 'string',
      server6_hostaddr: 'string',
      failover6_id: 3,
      failover6_name: 'string',
      scope6_name: 'string',
      scope6_space_id: 7,
      scope6_space_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      scope6_class_name: 'string',
      scope6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpScope6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpScope6(dhcpUpdateDhcpScope6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpScope6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScope6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScope6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScope6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScope6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScope6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScope6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopeCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopeCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScopeCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpScopeBodyParam = {
      server_id: 2,
      server_name: 'string',
      scope_id: 9,
      scope_net_addr: 'string',
      scope_net_mask: 'string',
      server_hostaddr: 'string',
      failover_id: 1,
      failover_name: 'string',
      scope_name: 'string',
      scope_space_id: 2,
      scope_space_name: 'string',
      sharednetwork_id: 7,
      sharednetwork_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      scope_class_name: 'string',
      scope_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpScope(dhcpUpdateDhcpScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopeInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopeInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScopeInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpScopeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpScopeList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpScopeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServer6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServer6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServer6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServer6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServer6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServer6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServer6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServer6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServer6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServerCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServerCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServerCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServerInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServerInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServerInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpServerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpServerList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpServerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpSharednetworkCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpSharednetworkCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpSharednetworkCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpSharednetworkBodyParam = {
      server_id: 9,
      server_name: 'string',
      sharednetwork_id: 5,
      sharednetwork_name: 'string',
      server_hostaddr: 'string',
      server_ip_id: 10,
      warnings: 'accept'
    };
    describe('#updateDhcpSharednetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpSharednetwork(dhcpUpdateDhcpSharednetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpSharednetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpSharednetworkInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpSharednetworkInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpSharednetworkInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpSharednetworkList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpSharednetworkList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpSharednetworkList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStatic6Count - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStatic6Count(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStatic6Count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpStatic6BodyParam = {
      server6_id: 7,
      server6_name: 'string',
      static6_client_duid: 'string',
      static6_id: 7,
      static6_mac_addr: 'string',
      static6_name: 'string',
      server6_hostaddr: 'string',
      server6_ip_id: 1,
      group6_id: 1,
      group6_name: 'string',
      static6_addr: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      static6_class_name: 'string',
      static6_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpStatic6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpStatic6(dhcpUpdateDhcpStatic6BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpStatic6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStatic6Info - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStatic6Info(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStatic6Info', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStatic6List - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStatic6List(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStatic6List', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStaticCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStaticCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStaticCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateDhcpStaticBodyParam = {
      server_id: 2,
      server_name: 'string',
      static_id: 1,
      static_mac_addr: 'string',
      server_hostaddr: 'string',
      server_ip_id: 5,
      group_id: 1,
      group_name: 'string',
      static_addr: 'string',
      static_name: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      static_class_name: 'string',
      static_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDhcpStatic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDhcpStatic(dhcpUpdateDhcpStaticBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateDhcpStatic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStaticInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStaticInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStaticInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcpStaticList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDhcpStaticList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'getDhcpStaticList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsAclBodyParam = {
      server_id: 10,
      server_name: 'string',
      acl_name: 'string',
      acl_value: 'string',
      server_hostaddr: 'string',
      warnings: 'accept'
    };
    describe('#createDnsAcl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsAcl(dnsCreateDnsAclBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsRrBodyParam = {
      server_id: 7,
      server_name: 'string',
      server_hostaddr: 'string',
      rr_name: 'string',
      rr_type: 'A',
      rr_value1: 'string',
      check_value: 'yes',
      view_id: 7,
      view_name: 'string',
      zone_id: 2,
      zone_name: 'string',
      zone_space_id: 6,
      rr_glue: 'string',
      rr_ttl: 5,
      rr_value2: 'string',
      rr_value3: 'string',
      rr_value4: 'string',
      rr_value5: 'string',
      rr_value6: 'string',
      rr_value7: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      rr_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDnsRr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsRr(dnsCreateDnsRrBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsRr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsViewBodyParam = {
      server_id: 8,
      server_name: 'string',
      view_name: 'string',
      server_hostaddr: 'string',
      view_allow_query: 'string',
      view_allow_recursion: 'string',
      view_allow_transfer: 'string',
      view_match_clients: 'string',
      view_match_to: 'string',
      view_order: 10,
      view_recursion: 'no',
      class_parameters_to_delete: [
        'string'
      ],
      view_class_name: 'string',
      view_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDnsView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsView(dnsCreateDnsViewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsViewparamBodyParam = {
      view_id: 9,
      viewparam_key: 'string',
      viewparam_is_array: 3,
      viewparam_value: 'string'
    };
    describe('#createDnsViewparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsViewparam(dnsCreateDnsViewparamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsViewparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsZoneBodyParam = {
      server_id: 7,
      server_name: 'string',
      view_id: 4,
      view_name: 'string',
      zone_name: 'string',
      zone_type: 'slave',
      server_hostaddr: 'string',
      zone_ad_integrated: 5,
      zone_allow_query: 'string',
      zone_allow_transfer: 'string',
      zone_allow_update: 'string',
      zone_also_notify: 'string',
      zone_forward: 'none',
      zone_forwarders: 'string',
      zone_is_rpz: 6,
      zone_masters: 'string',
      zone_notify: 'yes',
      zone_order: 8,
      zone_response_policy: 'string',
      zone_space_id: 2,
      zone_space_name: 'string',
      row_state: 6,
      zone_use_update_policy: 5,
      class_parameters_to_delete: [
        'string'
      ],
      zone_class_name: 'string',
      zone_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDnsZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsZone(dnsCreateDnsZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsCreateDnsZoneparamBodyParam = {
      zone_id: 3,
      zoneparam_key: 'string',
      zoneparam_is_array: 2,
      zoneparam_value: 'string'
    };
    describe('#createDnsZoneparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDnsZoneparam(dnsCreateDnsZoneparamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'createDnsZoneparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsAclCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsAclCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsAclCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsAclBodyParam = {
      server_id: 9,
      server_name: 'string',
      acl_id: 5,
      acl_name: 'string',
      acl_value: 'string',
      server_hostaddr: 'string',
      warnings: 'accept'
    };
    describe('#updateDnsAcl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsAcl(dnsUpdateDnsAclBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsAclInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsAclInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsAclInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsAclList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsAclList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsAclList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsRrCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsRrCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsRrCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsRrBodyParam = {
      server_id: 5,
      server_name: 'string',
      server_hostaddr: 'string',
      rr_id: 10,
      rr_name: 'string',
      rr_type: 'MG',
      rr_value1: 'string',
      check_value: 'no',
      view_id: 4,
      view_name: 'string',
      zone_id: 4,
      zone_name: 'string',
      zone_space_id: 1,
      rr_glue: 'string',
      rr_ttl: 5,
      rr_value2: 'string',
      rr_value3: 'string',
      rr_value4: 'string',
      rr_value5: 'string',
      rr_value6: 'string',
      rr_value7: 'string',
      class_parameters_to_delete: [
        'string'
      ],
      rr_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDnsRr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsRr(dnsUpdateDnsRrBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsRr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsRrInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsRrInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsRrInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsRrList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsRrList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsRrList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsServerCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsServerCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsServerCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsServerInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsServerInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsServerInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsServerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsServerList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsServerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsViewCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsViewCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsViewCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsViewBodyParam = {
      server_id: 2,
      server_name: 'string',
      view_id: 5,
      view_name: 'string',
      server_hostaddr: 'string',
      view_allow_query: 'string',
      view_allow_recursion: 'string',
      view_allow_transfer: 'string',
      view_match_clients: 'string',
      view_match_to: 'string',
      view_order: 10,
      view_recursion: 'no',
      class_parameters_to_delete: [
        'string'
      ],
      view_class_name: 'string',
      view_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDnsView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsView(dnsUpdateDnsViewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsViewInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsViewInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsViewInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsViewList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsViewList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsViewList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsViewparamCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsViewparamCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsViewparamCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsViewparamBodyParam = {
      view_id: 7,
      viewparam_key: 'string',
      viewparam_is_array: 8,
      viewparam_value: 'string'
    };
    describe('#updateDnsViewparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsViewparam(dnsUpdateDnsViewparamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsViewparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsViewparamList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsViewparamList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsViewparamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsZoneCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsZoneCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsZoneCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsZoneBodyParam = {
      server_id: 10,
      server_name: 'string',
      view_id: 5,
      view_name: 'string',
      zone_id: 8,
      zone_name: 'string',
      zone_type: 'forward',
      server_hostaddr: 'string',
      zone_ad_integrated: 9,
      zone_allow_query: 'string',
      zone_allow_transfer: 'string',
      zone_allow_update: 'string',
      zone_also_notify: 'string',
      zone_forward: 'first',
      zone_forwarders: 'string',
      zone_is_rpz: 2,
      zone_masters: 'string',
      zone_notify: 'no',
      zone_order: 4,
      zone_response_policy: 'string',
      zone_space_id: 2,
      zone_space_name: 'string',
      row_state: 8,
      zone_use_update_policy: 4,
      class_parameters_to_delete: [
        'string'
      ],
      zone_class_name: 'string',
      zone_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDnsZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsZone(dnsUpdateDnsZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsZoneInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsZoneInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsZoneInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsZoneList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsZoneList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsZoneList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsZoneparamCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsZoneparamCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsZoneparamCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsUpdateDnsZoneparamBodyParam = {
      zone_id: 9,
      zoneparam_key: 'string',
      zoneparam_is_array: 7,
      zoneparam_value: 'string'
    };
    describe('#updateDnsZoneparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDnsZoneparam(dnsUpdateDnsZoneparamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'updateDnsZoneparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsZoneparamList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDnsZoneparamList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'getDnsZoneparamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appCreateAppApplicationBodyParam = {
      application_fqdn: 'string',
      application_name: 'string',
      gslbserver_list: 'string',
      application_class_name: 'string',
      application_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'restrict'
        }
      ],
      class_parameters_to_delete: [
        'string'
      ],
      warnings: 'accept'
    };
    describe('#createAppApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAppApplication(appCreateAppApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'createAppApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appCreateAppNodeBodyParam = {
      application_fqdn: 'string',
      application_id: 9,
      application_name: 'string',
      pool_id: 5,
      pool_name: 'string',
      node_hostaddr: 'string',
      node_name: 'string',
      admin_status: 10,
      gslbserver_id: 5,
      healthcheck_name: 'ok',
      node_status: 1,
      node_weight: 'string',
      warnings: 'accept'
    };
    describe('#createAppNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAppNode(appCreateAppNodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'createAppNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appCreateAppPoolBodyParam = {
      application_fqdn: 'string',
      application_id: 10,
      application_name: 'string',
      pool_lb_mode: 'latency',
      pool_name: 'string',
      pool_type: 'ipv4',
      pool_affinity_session_time: 4,
      pool_affinity_state: 4,
      gslbserver_id: 9,
      pool_best_active_nodes: 7,
      warnings: 'accept'
    };
    describe('#createAppPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAppPool(appCreateAppPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'createAppPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppApplicationCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppApplicationCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppApplicationCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appUpdateAppApplicationBodyParam = {
      application_id: 7,
      application_fqdn: 'string',
      application_name: 'string',
      gslbserver_list: 'string',
      application_class_name: 'string',
      application_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      class_parameters_to_delete: [
        'string'
      ],
      warnings: 'accept'
    };
    describe('#updateAppApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAppApplication(appUpdateAppApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'updateAppApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppApplicationInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppApplicationInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppApplicationInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppApplicationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppApplicationList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppApplicationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppNodeCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppNodeCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppNodeCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appUpdateAppNodeBodyParam = {
      application_fqdn: 'string',
      application_id: 9,
      application_name: 'string',
      node_id: 3,
      pool_id: 3,
      pool_name: 'string',
      node_hostaddr: 'string',
      node_name: 'string',
      admin_status: 1,
      gslbserver_id: 8,
      healthcheck_name: 'ping',
      node_status: 0,
      node_weight: 'string',
      warnings: 'accept'
    };
    describe('#updateAppNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAppNode(appUpdateAppNodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'updateAppNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppNodeInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppNodeInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppNodeInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppNodeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppNodeList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppNodeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppPoolCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppPoolCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppPoolCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appUpdateAppPoolBodyParam = {
      application_fqdn: 'string',
      application_id: 1,
      application_name: 'string',
      pool_id: 8,
      pool_lb_mode: 'weighted',
      pool_name: 'string',
      pool_type: 'ipv6',
      pool_affinity_session_time: 3,
      pool_affinity_state: 6,
      gslbserver_id: 5,
      pool_best_active_nodes: 10,
      warnings: 'accept'
    };
    describe('#updateAppPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAppPool(appUpdateAppPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'updateAppPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppPoolInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppPoolInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppPoolInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppPoolList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppPoolList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'getAppPoolList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const guardianCreateGuardianPolicyBodyParam = {
      policy_name: 'string',
      description: 'string',
      server_list: 'string',
      warnings: 'accept'
    };
    describe('#createGuardianPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGuardianPolicy(guardianCreateGuardianPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'createGuardianPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGuardianPolicyCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGuardianPolicyCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'getGuardianPolicyCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const guardianUpdateGuardianPolicyBodyParam = {
      policy_id: 3,
      policy_name: 'string',
      description: 'string',
      server_list: 'string',
      warnings: 'accept'
    };
    describe('#updateGuardianPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGuardianPolicy(guardianUpdateGuardianPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'updateGuardianPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGuardianPolicyInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGuardianPolicyInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'getGuardianPolicyInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGuardianPolicyList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGuardianPolicyList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'getGuardianPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceCreateDeviceDeviceBodyParam = {
      device_name: 'string',
      device_addr: 'string',
      device_address_addr: 'string',
      device_space_id: 3,
      dev_id: 6,
      row_state: 3,
      class_parameters_to_delete: [
        'string'
      ],
      device_class_name: 'string',
      device_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDeviceDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceDevice(deviceCreateDeviceDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'createDeviceDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceCreateDeviceInterfaceBodyParam = {
      device_id: 4,
      device_name: 'string',
      interface_addr: 'string',
      interface_mac: 'string',
      interface_name: 'string',
      interface_type: 'port',
      interface_ip4_list: 'string',
      interface_ip6_list: 'string',
      port_id: 5,
      row_state: 2,
      space_id: 1,
      class_parameters_to_delete: [
        'string'
      ],
      interface_class_name: 'string',
      interface_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createDeviceInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceInterface(deviceCreateDeviceInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'createDeviceInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceCreateDeviceLinkBodyParam = {
      device1_name: 'string',
      device2_name: 'string',
      interface1_id: 1,
      interface1_name: 'string',
      interface2_id: 8,
      interface2_name: 'string',
      link_id: 8,
      link_auto_link: 9,
      warnings: 'accept'
    };
    describe('#createDeviceLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceLink(deviceCreateDeviceLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'createDeviceLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDeviceCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDeviceCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceDeviceCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceUpdateDeviceDeviceBodyParam = {
      device_id: 2,
      device_name: 'string',
      device_addr: 'string',
      device_address_addr: 'string',
      device_space_id: 5,
      dev_id: 2,
      row_state: 1,
      class_parameters_to_delete: [
        'string'
      ],
      device_class_name: 'string',
      device_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDeviceDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceDevice(deviceUpdateDeviceDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'updateDeviceDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDeviceInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDeviceInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceDeviceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceDeviceList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInterfaceCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceInterfaceCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceInterfaceCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceUpdateDeviceInterfaceBodyParam = {
      device_id: 4,
      device_name: 'string',
      interface_addr: 'string',
      interface_id: 3,
      interface_mac: 'string',
      interface_name: 'string',
      interface_type: 'interface',
      interface_ip4_list: 'string',
      interface_ip6_list: 'string',
      port_id: 3,
      row_state: 3,
      space_id: 2,
      class_parameters_to_delete: [
        'string'
      ],
      interface_class_name: 'string',
      interface_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateDeviceInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceInterface(deviceUpdateDeviceInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'updateDeviceInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInterfaceInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceInterfaceInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceInterfaceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceInterfaceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceInterfaceList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceInterfaceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLinkCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceLinkCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceLinkCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceUpdateDeviceLinkBodyParam = {
      device1_name: 'string',
      device2_name: 'string',
      interface1_id: 10,
      interface1_name: 'string',
      interface2_id: 6,
      interface2_name: 'string',
      link_id: 1,
      link_auto_link: 3,
      warnings: 'accept'
    };
    describe('#updateDeviceLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceLink(deviceUpdateDeviceLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'updateDeviceLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLinkList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceLinkList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceLinkList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanCreateVlanDomainBodyParam = {
      domain_name: 'string',
      domain_support_vxlan: 6,
      domain_description: 'string',
      domain_end_vlan_id: 8,
      domain_start_vlan_id: 4,
      class_parameters_to_delete: [
        'string'
      ],
      domain_class_name: 'string',
      domain_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createVlanDomain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVlanDomain(vlanCreateVlanDomainBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'createVlanDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanCreateVlanRangeBodyParam = {
      domain_id: 2,
      domain_name: 'string',
      range_name: 'string',
      range_description: 'string',
      range_disable_overlapping: 7,
      range_end_vlan_id: 2,
      range_start_vlan_id: 9,
      class_parameters_to_delete: [
        'string'
      ],
      range_class_name: 'string',
      range_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#createVlanRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVlanRange(vlanCreateVlanRangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'createVlanRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanCreateVlanVlanBodyParam = {
      domain_id: 6,
      domain_name: 'string',
      vlan_vlan_id: 3,
      range_id: 7,
      range_name: 'string',
      vlan_name: 'string',
      warnings: 'accept'
    };
    describe('#createVlanVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVlanVlan(vlanCreateVlanVlanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'createVlanVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanDomainCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanDomainCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanDomainCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanUpdateVlanDomainBodyParam = {
      domain_id: 9,
      domain_name: 'string',
      domain_support_vxlan: 2,
      domain_description: 'string',
      domain_end_vlan_id: 7,
      domain_start_vlan_id: 5,
      class_parameters_to_delete: [
        'string'
      ],
      domain_class_name: 'string',
      domain_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'inherited',
          propagation: 'restrict'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateVlanDomain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVlanDomain(vlanUpdateVlanDomainBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'updateVlanDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanDomainInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanDomainInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanDomainInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanDomainList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanDomainList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanDomainList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanRangeCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanRangeCount(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanRangeCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanUpdateVlanRangeBodyParam = {
      domain_id: 8,
      domain_name: 'string',
      range_id: 3,
      range_name: 'string',
      range_description: 'string',
      range_disable_overlapping: 3,
      range_end_vlan_id: 1,
      range_start_vlan_id: 10,
      class_parameters_to_delete: [
        'string'
      ],
      range_class_name: 'string',
      range_class_parameters: [
        {
          name: 'string',
          value: 'string',
          inheritance: 'set',
          propagation: 'propagate'
        }
      ],
      warnings: 'accept'
    };
    describe('#updateVlanRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVlanRange(vlanUpdateVlanRangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'updateVlanRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanRangeInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanRangeInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanRangeInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanRangeList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanRangeList(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanRangeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanVlanCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanVlanCount(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanVlanCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanUpdateVlanVlanBodyParam = {
      domain_id: 3,
      domain_name: 'string',
      vlan_id: 8,
      vlan_vlan_id: 1,
      range_id: 9,
      range_name: 'string',
      vlan_name: 'string',
      warnings: 'accept'
    };
    describe('#updateVlanVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVlanVlan(vlanUpdateVlanVlanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'updateVlanVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanVlanInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanVlanInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanVlanInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVlanVlanList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVlanVlanList(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.success);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'getVlanVlanList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAddress6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamAddress6(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamAddress6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamAddress(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAlias6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamAlias6(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamAlias6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAlias - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamAlias(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamNetwork6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamNetwork6(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamNetwork6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPool6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamPool6(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamPool6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamPool(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamSpace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamSpace(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamSpace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpamNetwork(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpGroup(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpRange6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpRange6(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpRange6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpRange(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpScope6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpScope6(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpScope6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpScope(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpStatic6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpStatic6(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpStatic6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpStatic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDhcpStatic(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'deleteDhcpStatic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsAcl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsAcl(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsRr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsRr(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsRr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsView(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsViewparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsViewparam(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsViewparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsZone(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnsZoneparam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDnsZoneparam(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dns', 'deleteDnsZoneparam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAppApplication(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'deleteAppApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAppNode(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'deleteAppNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAppPool(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('App', 'deleteAppPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGuardianPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteGuardianPolicy(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Guardian', 'deleteGuardianPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceDevice(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'deleteDeviceDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceInterface(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'deleteDeviceInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceLink(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'deleteDeviceLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanDomain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVlanDomain(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'deleteVlanDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanRange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVlanRange(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'deleteVlanRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVlanVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVlanVlan(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'deleteVlanVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
