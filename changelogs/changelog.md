
## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!1

---

## 0.1.1 [08-03-2021]

- Initial Commit

See commit 8998d7b

---
