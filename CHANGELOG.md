
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:23PM

See merge request itentialopensource/adapters/adapter-efficientip_solidserver!13

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-efficientip_solidserver!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:34PM

See merge request itentialopensource/adapters/adapter-efficientip_solidserver!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:46PM

See merge request itentialopensource/adapters/adapter-efficientip_solidserver!9

---

## 0.4.0 [05-10-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!8

---

## 0.3.5 [03-26-2024]

* Changes made at 2024.03.26_14:43PM

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!7

---

## 0.3.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:02PM

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:11PM

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!4

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:39PM

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-efficientip_solidserver!1

---

## 0.1.1 [08-03-2021]

- Initial Commit

See commit 8998d7b

---
