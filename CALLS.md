## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Efficient iP SOLIDserver. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Efficient iP SOLIDserver.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Efficient iP SOLIDserver. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getIpamSpaceList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the spaces</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamSpaceCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of spaces</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamSpaceInfo(spaceId, callback)</td>
    <td style="padding:15px">Display the properties of a space</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamSpace(body, callback)</td>
    <td style="padding:15px">Add a space</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamSpace(body, callback)</td>
    <td style="padding:15px">Edit a space</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamSpace(spaceId, spaceName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a space</td>
    <td style="padding:15px">{base_path}/{version}/ipam_space_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetworkList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv4 block/subnet-type networks</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetworkCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv4 block/subnet-type networks</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetworkInfo(networkId, callback)</td>
    <td style="padding:15px">Display the properties of an IPv4 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamNetwork(body, callback)</td>
    <td style="padding:15px">Add an IPv4 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamNetwork(body, callback)</td>
    <td style="padding:15px">Edit an IPv4 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamNetwork(parentNetworkId, spaceId, spaceName, networkAddr, networkEndAddr, networkId, networkMask, networkPrefix, networkSize, relativePosition, networkLevel, networkName, useReversedRelativePosition, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv4 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPoolList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv4 pools</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPoolCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv4 pools</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPoolInfo(poolId, callback)</td>
    <td style="padding:15px">Display the properties of an IPv4 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamPool(body, callback)</td>
    <td style="padding:15px">Add an IPv4 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamPool(body, callback)</td>
    <td style="padding:15px">Edit an IPv4 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamPool(poolEndIpAddr, poolId, poolSize, spaceId, spaceName, poolStartIpAddr, networkId, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv4 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv4 addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv4 addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddressInfo(addressId, callback)</td>
    <td style="padding:15px">Display the properties of an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamAddress(body, callback)</td>
    <td style="padding:15px">Add an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamAddress(body, callback)</td>
    <td style="padding:15px">Edit an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAddress(addressHostaddr, addressId, spaceId, spaceName, addressName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAliasList(orderby, where, limit, offset, addressId, tags, callback)</td>
    <td style="padding:15px">List the aliases of an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAliasCount(where, addressId, tags, callback)</td>
    <td style="padding:15px">Count the number of aliases of an IPv4 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamAlias(body, callback)</td>
    <td style="padding:15px">Add an IPv4 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamAlias(body, callback)</td>
    <td style="padding:15px">Edit an IPv4 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAlias(aliasHostaddr, addressId, addressName, aliasId, spaceId, spaceName, aliasType, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv4 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetwork6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv6 block/subnet-type networks</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetwork6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv6 block/subnet-type networks</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamNetwork6Info(network6Id, callback)</td>
    <td style="padding:15px">Display the properties of an IPv6 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamNetwork6(body, callback)</td>
    <td style="padding:15px">Add an IPv6 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamNetwork6(body, callback)</td>
    <td style="padding:15px">Edit an IPv6 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamNetwork6(parentNetwork6Id, spaceId, spaceName, network6Addr, network6EndAddr, network6Id, network6Prefix, relativePosition, network6Name, networkLevel, useReversedRelativePosition, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv6 block/subnet-type network</td>
    <td style="padding:15px">{base_path}/{version}/ipam_network6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPool6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv6 pools</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPool6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv6 pools</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPool6Info(pool6Id, callback)</td>
    <td style="padding:15px">Display the properties of an IPv6 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamPool6(body, callback)</td>
    <td style="padding:15px">Add an IPv6 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamPool6(body, callback)</td>
    <td style="padding:15px">Edit an IPv6 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamPool6(pool6EndIpAddr, pool6Id, spaceId, spaceName, pool6StartIpAddr, network6Id, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv6 pool</td>
    <td style="padding:15px">{base_path}/{version}/ipam_pool6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddress6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the IPv6 addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddress6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of IPv6 addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAddress6Info(address6Id, callback)</td>
    <td style="padding:15px">Display the properties of an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamAddress6(body, callback)</td>
    <td style="padding:15px">Add an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamAddress6(body, callback)</td>
    <td style="padding:15px">Edit an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAddress6(address6Hostaddr, address6Id, spaceId, spaceName, address6Name, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_address6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAlias6List(orderby, where, limit, offset, address6Id, tags, callback)</td>
    <td style="padding:15px">List the aliases of an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAlias6Count(where, address6Id, tags, callback)</td>
    <td style="padding:15px">Count the number of aliases of an IPv6 address</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamAlias6(body, callback)</td>
    <td style="padding:15px">Add an IPv6 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamAlias6(body, callback)</td>
    <td style="padding:15px">Edit an IPv6 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAlias6(alias6Hostaddr, address6Id, address6Name, alias6Id, spaceId, spaceName, alias6Type, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an IPv6 address alias</td>
    <td style="padding:15px">{base_path}/{version}/ipam_alias6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServerList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServerCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServerInfo(serverId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpFailoverList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DHCPv4 failover channels</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_failover_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpFailoverCount(where, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 failover channels</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_failover_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpFailoverInfo(failoverId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 failover channel</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_failover_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpGroupList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 groups</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpGroupCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 groups</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpGroupInfo(groupId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpGroup(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpGroup(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpGroup(serverId, serverName, groupId, groupName, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv4 group</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpSharednetworkList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DHCPv4 shared networks</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_sharednetwork_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpSharednetworkCount(where, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 shared networks</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_sharednetwork_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpSharednetworkInfo(sharednetworkId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 shared network</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_sharednetwork_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpSharednetwork(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 shared network</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_sharednetwork_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpSharednetwork(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv4 shared network</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_sharednetwork_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopeList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 scopes</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopeCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 scopes</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScopeInfo(scopeId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpScope(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpScope(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv4 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpScope(serverId, serverName, scopeId, scopeNetAddr, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv4 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRangeList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 ranges</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRangeCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 ranges</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRangeInfo(rangeId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpRange(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpRange(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv4 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpRange(serverId, serverName, rangeEndAddr, rangeId, rangeName, rangeStartAddr, scopeId, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv4 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLeaseList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_lease_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLeaseCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_lease_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLeaseInfo(leaseId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 lease</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_lease_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStaticList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv4 statics</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStaticCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv4 statics</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStaticInfo(staticId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv4 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpStatic(body, callback)</td>
    <td style="padding:15px">Add a DHCPv4 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpStatic(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv4 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpStatic(serverId, serverName, staticAddr, staticId, scopeId, serverHostaddr, staticMacAddr, staticName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv4 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DHCP ACLs</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_acl_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclCount(where, callback)</td>
    <td style="padding:15px">Count the number of DHCP ACLs</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_acl_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclInfo(aclId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCP ACL</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_acl_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclentryList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DHCP ACL entries</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_aclentry_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclentryCount(where, callback)</td>
    <td style="padding:15px">Count the number of DHCP ACL entries</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_aclentry_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpAclentryInfo(aclentryId, callback)</td>
    <td style="padding:15px">Display the properties of a DHCP ACL entry</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_aclentry_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServer6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServer6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv6 servers</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpServer6Info(server6Id, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv6 server</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_server6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpGroup6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 groups</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_group6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScope6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 scopes</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScope6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv6 scopes</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpScope6Info(scope6Id, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv6 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpScope6(body, callback)</td>
    <td style="padding:15px">Add a DHCPv6 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpScope6(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv6 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpScope6(server6Id, server6Name, scope6Id, scope6StartAddr, server6Hostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv6 scope</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_scope6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRange6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 ranges</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRange6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv6 ranges</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpRange6Info(range6Id, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv6 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpRange6(body, callback)</td>
    <td style="padding:15px">Add a DHCPv6 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpRange6(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv6 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpRange6(server6Id, server6Name, range6EndAddr, range6Id, range6StartAddr, scope6Id, server6Hostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv6 range</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_range6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLease6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 lesases</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_lease6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpLease6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv6 leases</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_lease6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStatic6List(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DHCPv6 statics</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStatic6Count(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DHCPv6 statics</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcpStatic6Info(static6Id, callback)</td>
    <td style="padding:15px">Display the properties of a DHCPv6 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpStatic6(body, callback)</td>
    <td style="padding:15px">Add a DHCPv6 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDhcpStatic6(body, callback)</td>
    <td style="padding:15px">Edit a DHCPv6 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpStatic6(server6Id, server6Name, static6Addr, static6Id, static6Name, scope6Id, server6Hostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DHCPv6 static</td>
    <td style="padding:15px">{base_path}/{version}/dhcp_static6_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsServerList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DNS servers</td>
    <td style="padding:15px">{base_path}/{version}/dns_server_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsServerCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of DNS servers</td>
    <td style="padding:15px">{base_path}/{version}/dns_server_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsServerInfo(serverId, callback)</td>
    <td style="padding:15px">Display the properties of a DNS server</td>
    <td style="padding:15px">{base_path}/{version}/dns_server_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the views</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of views</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewInfo(viewId, callback)</td>
    <td style="padding:15px">Display the properties of a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsView(body, callback)</td>
    <td style="padding:15px">Add a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsView(body, callback)</td>
    <td style="padding:15px">Edit a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsView(serverId, serverName, viewId, viewName, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_view_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZoneList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the DNS options of a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZoneCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of zones</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZoneInfo(zoneId, callback)</td>
    <td style="padding:15px">Display the properties of a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsZone(body, callback)</td>
    <td style="padding:15px">Add a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsZone(body, callback)</td>
    <td style="padding:15px">Edit a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsZone(serverId, serverName, viewId, viewName, zoneId, zoneName, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zone_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsRrList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the resource records</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsRrCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of resource records</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsRrInfo(rrId, callback)</td>
    <td style="padding:15px">Display the properties of a resource record</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsRr(body, callback)</td>
    <td style="padding:15px">Add a resource record</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsRr(body, callback)</td>
    <td style="padding:15px">Edit a resource record</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsRr(serverId, serverName, zoneId, zoneName, serverHostaddr, rrId, rrName, viewId, viewName, zoneSpaceId, rrType = 'A', rrValue1, rrValue2, rrValue3, rrValue4, rrValue5, rrValue6, rrValue7, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a resource record</td>
    <td style="padding:15px">{base_path}/{version}/dns_rr_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsAclList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DNS ACLs</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsAclCount(where, callback)</td>
    <td style="padding:15px">Count the number of DNS ACLs</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsAclInfo(aclId, callback)</td>
    <td style="padding:15px">Display the properties of a DNS ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsAcl(body, callback)</td>
    <td style="padding:15px">Add a DNS ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsAcl(body, callback)</td>
    <td style="padding:15px">Edit a DNS ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsAcl(aclId, serverId, serverName, aclName, serverHostaddr, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a DNS ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns_acl_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewparamList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DNS options of a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_viewparam_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewparamCount(where, callback)</td>
    <td style="padding:15px">Count the number of DNS options of a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_viewparam_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsViewparam(body, callback)</td>
    <td style="padding:15px">Add a DNS option on a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_viewparam_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsViewparam(body, callback)</td>
    <td style="padding:15px">Edit a DNS option on a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_viewparam_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsViewparam(viewId, viewparamKey, callback)</td>
    <td style="padding:15px">Delete a DNS option from a view</td>
    <td style="padding:15px">{base_path}/{version}/dns_viewparam_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZoneparamList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the DNS options of a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zoneparam_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsZoneparamCount(where, callback)</td>
    <td style="padding:15px">Count the number of DNS options of a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zoneparam_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnsZoneparam(body, callback)</td>
    <td style="padding:15px">Add a DNS option on a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zoneparam_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnsZoneparam(body, callback)</td>
    <td style="padding:15px">Edit a DNS option on a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zoneparam_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsZoneparam(zoneId, zoneparamKey, callback)</td>
    <td style="padding:15px">Delete a DNS option from a zone</td>
    <td style="padding:15px">{base_path}/{version}/dns_zoneparam_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppApplicationList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the applications</td>
    <td style="padding:15px">{base_path}/{version}/app_application_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppApplicationCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of applications</td>
    <td style="padding:15px">{base_path}/{version}/app_application_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppApplicationInfo(applicationId, callback)</td>
    <td style="padding:15px">Display the properties of an application</td>
    <td style="padding:15px">{base_path}/{version}/app_application_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppApplication(body, callback)</td>
    <td style="padding:15px">Add an application</td>
    <td style="padding:15px">{base_path}/{version}/app_application_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppApplication(body, callback)</td>
    <td style="padding:15px">Edit an application</td>
    <td style="padding:15px">{base_path}/{version}/app_application_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppApplication(applicationId, applicationFqdn, applicationName, gslbserverId, gslbserverName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete an application</td>
    <td style="padding:15px">{base_path}/{version}/app_application_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppPoolList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the pools</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppPoolCount(where, callback)</td>
    <td style="padding:15px">Count the number of pools</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppPoolInfo(poolId, callback)</td>
    <td style="padding:15px">Display the properties of a pool</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppPool(body, callback)</td>
    <td style="padding:15px">Add a pool</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppPool(body, callback)</td>
    <td style="padding:15px">Edit a pool</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppPool(applicationFqdn, applicationId, applicationName, poolId, poolName, gslbserverId, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a pool</td>
    <td style="padding:15px">{base_path}/{version}/app_pool_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppNodeList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the nodes</td>
    <td style="padding:15px">{base_path}/{version}/app_node_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppNodeCount(where, callback)</td>
    <td style="padding:15px">Count the number of nodes</td>
    <td style="padding:15px">{base_path}/{version}/app_node_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppNodeInfo(nodeId, callback)</td>
    <td style="padding:15px">Display the properties of a node</td>
    <td style="padding:15px">{base_path}/{version}/app_node_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppNode(body, callback)</td>
    <td style="padding:15px">Add a node</td>
    <td style="padding:15px">{base_path}/{version}/app_node_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAppNode(body, callback)</td>
    <td style="padding:15px">Edit a node</td>
    <td style="padding:15px">{base_path}/{version}/app_node_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppNode(applicationFqdn, applicationId, applicationName, nodeId, poolId, poolName, nodeName, gslbserverId, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a node</td>
    <td style="padding:15px">{base_path}/{version}/app_node_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGuardianPolicyList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the policies</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGuardianPolicyCount(where, callback)</td>
    <td style="padding:15px">Count the number of policies</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGuardianPolicyInfo(policyId, callback)</td>
    <td style="padding:15px">Display the properties of a policy</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGuardianPolicy(body, callback)</td>
    <td style="padding:15px">Add a policy</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGuardianPolicy(body, callback)</td>
    <td style="padding:15px">Edit a policy</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGuardianPolicy(serverId, serverName, policyId, policyName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a policy</td>
    <td style="padding:15px">{base_path}/{version}/guardian_policy_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDeviceList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the Device Manager devices</td>
    <td style="padding:15px">{base_path}/{version}/device_device_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDeviceCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of Device Manager devices</td>
    <td style="padding:15px">{base_path}/{version}/device_device_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDeviceInfo(deviceId, callback)</td>
    <td style="padding:15px">Display the properties of a Device Manager device</td>
    <td style="padding:15px">{base_path}/{version}/device_device_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceDevice(body, callback)</td>
    <td style="padding:15px">Add a Device Manager device</td>
    <td style="padding:15px">{base_path}/{version}/device_device_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceDevice(body, callback)</td>
    <td style="padding:15px">Add a Device Manager device</td>
    <td style="padding:15px">{base_path}/{version}/device_device_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceDevice(deviceId, deviceName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a Device Manager device</td>
    <td style="padding:15px">{base_path}/{version}/device_device_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInterfaceList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the Device Manager ports & interfaces</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInterfaceCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number Device Manager ports & interfaces</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceInterfaceInfo(interfaceId, callback)</td>
    <td style="padding:15px">Display the properties of a Device Manager port or interface</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceInterface(body, callback)</td>
    <td style="padding:15px">Add a Device Manager port or interface</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceInterface(body, callback)</td>
    <td style="padding:15px">Add a Device Manager port or interface</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceInterface(deviceId, deviceName, interfaceId, interfaceName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a Device Manager port or interface</td>
    <td style="padding:15px">{base_path}/{version}/device_interface_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLinkList(orderby, where, limit, offset, interfaceId, callback)</td>
    <td style="padding:15px">List Device Manager ports & interfaces</td>
    <td style="padding:15px">{base_path}/{version}/device_link_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLinkCount(where, interfaceId, callback)</td>
    <td style="padding:15px">Count the number of links between Device Manager devices</td>
    <td style="padding:15px">{base_path}/{version}/device_link_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceLink(body, callback)</td>
    <td style="padding:15px">Link two Device Manager devices using their ports and/or interfaces</td>
    <td style="padding:15px">{base_path}/{version}/device_link_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceLink(body, callback)</td>
    <td style="padding:15px">Link two Device Manager devices using their ports and/or interfaces</td>
    <td style="padding:15px">{base_path}/{version}/device_link_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceLink(device1Name, device2Name, interface1Id, interface1Name, interface2Id, interface2Name, linkId, linkAutoLink, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a link between two Device Manager devices</td>
    <td style="padding:15px">{base_path}/{version}/device_link_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanDomainList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the VLAN domains</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanDomainCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of VLAN domains</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanDomainInfo(domainId, callback)</td>
    <td style="padding:15px">Display the properties of a VLAN domain</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlanDomain(body, callback)</td>
    <td style="padding:15px">Add a VLAN domain</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlanDomain(body, callback)</td>
    <td style="padding:15px">Edit a VLAN domain</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanDomain(domainId, domainName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a VLAN domain</td>
    <td style="padding:15px">{base_path}/{version}/vlan_domain_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanRangeList(orderby, where, limit, offset, tags, callback)</td>
    <td style="padding:15px">List the VLAN ranges</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanRangeCount(where, tags, callback)</td>
    <td style="padding:15px">Count the number of VLAN ranges</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanRangeInfo(rangeId, callback)</td>
    <td style="padding:15px">Display the properties of a VLAN range</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlanRange(body, callback)</td>
    <td style="padding:15px">Add a VLAN range</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlanRange(body, callback)</td>
    <td style="padding:15px">Edit a VLAN range</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanRange(domainId, domainName, rangeId, rangeName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a VLAN range</td>
    <td style="padding:15px">{base_path}/{version}/vlan_range_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanVlanList(orderby, where, limit, offset, callback)</td>
    <td style="padding:15px">List the VLANs</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanVlanCount(where, callback)</td>
    <td style="padding:15px">Count the number of VLANs</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanVlanInfo(vlanId, callback)</td>
    <td style="padding:15px">Display the properties of a VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlanVlan(body, callback)</td>
    <td style="padding:15px">Add a VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlanVlan(body, callback)</td>
    <td style="padding:15px">Edit a VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanVlan(domainId, domainName, vlanId, vlanVlanId, rangeId, rangeName, vlanName, warnings = 'accept', callback)</td>
    <td style="padding:15px">Delete a VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan_vlan_delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
