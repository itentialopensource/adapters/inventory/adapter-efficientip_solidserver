# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Efficientip_solidserver System. The API that was used to build the adapter for Efficientip_solidserver is usually available in the report directory of this adapter. The adapter utilizes the Efficientip_solidserver API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Efficient iP SOLIDserver adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Efficient iP SOLIDserver. With this adapter you have the ability to perform operations on:

- Devices
- DHCP
- DNS
- IPAM
- VLAN

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
